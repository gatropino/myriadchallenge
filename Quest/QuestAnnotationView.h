//
//  QuestAnnotationView.h
//  Quest
//
//  Created by Greg Tropino on 5/9/14.
//
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface QuestAnnotationView : NSObject <MKAnnotation>

@property (nonatomic, copy) NSString *title;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *subtitle;
@property (strong, nonatomic) NSString *questGiverOrLocation;

- (id)initWithTitle:(NSString *)title subtitle:(NSString *)questDescription atCoodinates:(CLLocationCoordinate2D)coord giverOrLocation:(NSString*)questIsGiverOrLocation;

@end
