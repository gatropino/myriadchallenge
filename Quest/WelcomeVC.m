//
//  WelcomeVC.m
//  Quest
//
//  Created by Greg Tropino on 5/6/14.
//
//

#import "WelcomeVC.h"
#import "PlayerSettings.h"
#import "QuestListTableView.h"
#import <Parse/Parse.h>
#import "Constants.h"

@interface WelcomeVC ()

@end

@implementation WelcomeVC
{
    UITextField *lastTextFieldSelected;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _currentSettings = [[PlayerSettings alloc]init];
    [self loadSettings];
    NSLog(@"%@", good);
    _buttonInvsForKeyboardDismiss.hidden = YES;
    
    //if login name was previously set to be saved, then load screen accordingly
    if (_currentSettings.loginName)
    {
        _switchRememberUsername.on = YES;
        _labelOnOffSwitchDisplay.text = @"On";
        _labelOnOffSwitchDisplay.textColor = [UIColor blackColor];
        _textFieldUsername.text = _currentSettings.loginName;
    }
    else
    {
        _switchRememberUsername.on = NO;
        _labelOnOffSwitchDisplay.text = @"Off";
        _labelOnOffSwitchDisplay.textColor = [UIColor redColor];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    PFUser *currentUser = [PFUser currentUser];
    if (currentUser)
    {
        [self loadParseSettings];
        NSLog(@" User found, loading settings");
    }
    else
    {
        NSLog(@"User not found");
        _textFieldPassword.text = @"";
    }
}

- (IBAction)changedStateOfSwitch:(UISwitch*)sender
{
    if (sender.on)
    {
        _labelOnOffSwitchDisplay.text = @"On";
        _labelOnOffSwitchDisplay.textColor = [UIColor blackColor];
    }
    else
    {
        _labelOnOffSwitchDisplay.text = @"Off";
        _labelOnOffSwitchDisplay.textColor = [UIColor redColor];
    }
}

#pragma methods handling login
- (IBAction)pressedLogIn:(id)sender
{
    [self loginVerification];
}

-(void)loginVerification
{
    //Checking to see if user exists first to determine if its faulty user info or faulty password, any way to make this query and the PFUser login in one connection, or have login error give better info?
    if ([_textFieldUsername.text isEqualToString:@""])
    {
        _labelInvalidUser.hidden = NO;
    }
    else
    {
        PFQuery *query = [PFUser query];
        [query whereKey:@"username" equalTo:_textFieldUsername.text];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
         {
             if (!error)
             {
                 if ([objects count])
                 {
                     _labelInvalidUser.hidden = YES;
                     [PFUser logInWithUsernameInBackground:_textFieldUsername.text password:_textFieldPassword.text block:^(PFUser *user, NSError *error)
                      {
                          if (user)
                          {
                              _labelInvalidUser.hidden = YES;
                              _labelInvalidPassword.hidden = YES;
                              
                              //If user set to remember username, then it will save to plist for use later, only saves when login valid
                              if (_switchRememberUsername.on)
                              {
                                  _currentSettings.loginName = _textFieldUsername.text;
                                  [self saveSettings];
                              }
                              else
                              {
                                  _currentSettings.loginName = NULL;
                                  [self saveSettings];
                              }
                              
                              if ([[user objectForKey:alignment]intValue] == 0)
                              {
                                  _currentSettings.heroAlignment = good;
                                  NSLog(@"User was set to being good");
                              }
                              else if ([[user objectForKey:alignment]intValue] == 1)
                              {
                                  _currentSettings.heroAlignment = neutral;
                              }
                              else if ([[user objectForKey:alignment]intValue] == 2)
                              {
                                  _currentSettings.heroAlignment = evil;
                              }
                              else
                              {
                                  _currentSettings.heroAlignment = neutral;
                                  NSLog(@"User didn't have any alignment set");
                                  //set default currentSetting.heroAlignment to neutral?
                              }
                              _currentSettings.heroName = user[@"name"];
                              
                              PFFile *imageFile = user[@"customUserImage"];
                              if (imageFile)
                              {
                                  [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                                      if (!error)
                                      {
                                          NSLog(@"Got custom user image!");
                                          _currentSettings.heroImage = [UIImage imageWithData:data];
                                          [self performSegueWithIdentifier:@"tableVC" sender:self];
                                      }
                                      else
                                      {
                                          NSLog(@"Error getting customUser image");
                                          [self performSegueWithIdentifier:@"tableVC" sender:self];
                                      }
                                  }];
                              }
                              else
                              {
                                  NSLog(@"No customUser image available");
                                  [self performSegueWithIdentifier:@"tableVC" sender:self];
                              }
                          }
                          else
                          {
                              //Login failed, since username matched previously, must be password
                              _labelInvalidPassword.hidden = NO;
                              NSLog(@"%@", error);
                          }
                      }];
                 }
                 else
                 {
                     //returned saying no users matched what was entered
                     _labelInvalidUser.hidden = NO;
                 }
             }
         }];
    }
}

- (IBAction)pressedLoginWithFB:(id)sender
{
    [PFFacebookUtils logInWithPermissions:nil block:^(PFUser *user, NSError *error)
     {
         if (!user)
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Registration Error" message: @"Process was cancelled or error occurred registering, please try again or register manually!" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [alert show];
         }
         else if (user.isNew)
         {
             //how to stop process or prompt them for hero name/alignment if they pressed this without registering???
             //send to a different view, maybe settingsVC if they are new user??
             user[alignment] = [NSNumber numberWithInt:1];
             [user saveInBackground];
             
             _currentSettings.heroAlignment = neutral;
             
             [self performSegueWithIdentifier:@"tableVC" sender:self];
             
             NSLog(@"User signed up and logged in through Facebook!");
         }
         else
         {
             _currentSettings.heroName = user[@"name"];
             int numericalAlignmentValue = [user[alignment]intValue];
             if (numericalAlignmentValue == 0)
             {
                 _currentSettings.heroAlignment = good;
             }
             else if (numericalAlignmentValue == 1)
             {
                 _currentSettings.heroAlignment = neutral;
             }
             else if (numericalAlignmentValue == 2)
             {
                 _currentSettings.heroAlignment = evil;
             }
             else
             {
                 _currentSettings.heroAlignment = neutral;
             }
             PFFile *imageFile = user[@"customUserImage"];
             if (imageFile)
             {
                 [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                     if (!error)
                     {
                         NSLog(@"logged in with fb with user image");
                         _currentSettings.heroImage = [UIImage imageWithData:data];
                         [self performSegueWithIdentifier:@"tableVC" sender:self];
                     }
                     else
                     {
                         NSLog(@"logged in with fb but error downloading image");
                         [self performSegueWithIdentifier:@"tableVC" sender:self];
                     }
                 }];
             }
             else
             {
                 NSLog(@"Logged in through facebook with no user image associated with it");
                 [self performSegueWithIdentifier:@"tableVC" sender:self];
             }
         }
     }];
}

#pragma mark textFieldRelated methods
- (IBAction)pressedInvsDismissKeyboard:(id)sender
{
    [lastTextFieldSelected resignFirstResponder];
    _buttonInvsForKeyboardDismiss.hidden = YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    _buttonInvsForKeyboardDismiss.hidden = NO;
    lastTextFieldSelected = textField;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    _buttonInvsForKeyboardDismiss.hidden = YES;
    [self loginVerification];
    return YES;
}

#pragma mark pList/parse saving and loading
- (void)saveSettings
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_currentSettings.loginName forKey:@"loginName"];
    [defaults synchronize];
}

-(void) loadSettings
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _currentSettings.loginName = [defaults objectForKey:@"loginName"];
}

-(void)loadParseSettings
{
    PFUser *currentUser = [PFUser currentUser];
    
    _currentSettings.heroName = currentUser[@"name"];
    int numericalAlignmentValue = [currentUser[alignment]intValue];
    if (numericalAlignmentValue == 0)
    {
        _currentSettings.heroAlignment = good;
    }
    else if (numericalAlignmentValue == 1)
    {
        _currentSettings.heroAlignment = neutral;
    }
    else if (numericalAlignmentValue == 2)
    {
        _currentSettings.heroAlignment = evil;
    }
    else
    {
        _currentSettings.heroAlignment = neutral;
    }
    NSLog(@"loading with %@ %@", _currentSettings.heroAlignment, _currentSettings.heroName);
    
    PFFile *imageFile = currentUser[@"customUserImage"];
    if (imageFile)
    {
        [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error)
         {
             if (!error)
             {
                 NSLog(@"image found for user, moving forward with it");
                 _currentSettings.heroImage = [UIImage imageWithData:data];
                 [self performSegueWithIdentifier:@"tableVC" sender:self];
             }
             else
             {
                 NSLog(@"error downloading image, moving forward anyway");
                 [self performSegueWithIdentifier:@"tableVC" sender:self];
             }
         }];
    }
    else
    {
        NSLog(@"No image associated with user, going to tablevc");
        [self performSegueWithIdentifier:@"tableVC" sender:self];
    }
    NSLog(@"At end of loading parse settings");
}


#pragma mark navigation & passing info
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"tableVC"])
    {
        UINavigationController *navController = [segue destinationViewController];
        QuestListTableView *qltv = (QuestListTableView*)navController.topViewController;
        qltv.currentSettings = _currentSettings;
    }
}
@end
