//
//  SignUpVC.h
//  Quest
//
//  Created by Greg Tropino on 5/10/14.
//
//

#import <UIKit/UIKit.h>
#import "PlayerSettings.h"
#import "CameraVC.h"

@interface SignUpVC : UIViewController <UITextFieldDelegate, passImageBackDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textFieldUserName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldFirstPassword;
@property (weak, nonatomic) IBOutlet UITextField *textFieldSecondPassword;
@property (weak, nonatomic) IBOutlet UITextField *textFieldHeroName;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedHeroAlignment;
@property (weak, nonatomic) IBOutlet UILabel *labelPasswordsDontMatch;
@property (weak, nonatomic) IBOutlet UILabel *labelMustEnterUsername;
@property (weak, nonatomic) IBOutlet UILabel *labelMustEnterHeroName;
@property (weak, nonatomic) IBOutlet UIButton *buttonUserImageHere;

- (IBAction)pressedCancel:(id)sender;
- (IBAction)pressedRegister:(id)sender;
- (IBAction)pressedInvsKeyboardDismiss:(id)sender;
- (IBAction)pressedRegisterWithFacebook:(id)sender;

@end
