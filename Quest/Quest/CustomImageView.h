//
//  CustomImageView.h
//  Quest
//
//  Created by Greg Tropino on 5/18/14.
//
//

#import <UIKit/UIKit.h>

@interface CustomImageView : UIImageView

@property (nonatomic, strong, setter = setImage:) UIImage *image;

@end
