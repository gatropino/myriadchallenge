//
//  SignUpVC.m
//  Quest
//
//  Created by Greg Tropino on 5/10/14.
//
//

#import "SignUpVC.h"
#import <Parse/Parse.h>
#import "QuestListTableView.h"
#import "WelcomeVC.h"
#import "Constants.h"

@interface SignUpVC ()

@end

@implementation SignUpVC
{
    UITextField *currentTextFieldHoldingKeyboard;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (IBAction)pressedCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)passedImageBack:(UIImage *)imagePassed
{
    _buttonUserImageHere.imageView.image = imagePassed;
    //assigning tag number to figure out later if a image was placed here
    _buttonUserImageHere.tag = 2;
}

- (IBAction)pressedRegister:(id)sender
{
    if ([_textFieldUserName.text isEqualToString:@""])
    {
        _labelMustEnterUsername.hidden = NO;
        _labelPasswordsDontMatch.hidden = YES;
        _labelMustEnterHeroName.hidden = YES;
    }
    else if (![_textFieldFirstPassword.text isEqualToString:_textFieldSecondPassword.text] || [_textFieldFirstPassword.text isEqualToString:@""])
    {
        _labelPasswordsDontMatch.hidden = NO;
        _labelMustEnterUsername.hidden = YES;
        _labelMustEnterHeroName.hidden = YES;
    }
    else if ([_textFieldHeroName.text isEqualToString:@""])
    {
        _labelMustEnterHeroName.hidden = NO;
        _labelPasswordsDontMatch.hidden = YES;
        _labelMustEnterUsername.hidden = YES;
    }
    else
    {
        _labelPasswordsDontMatch.hidden = YES;
        _labelMustEnterUsername.hidden = YES;
        _labelMustEnterHeroName.hidden = YES;
        
        PFQuery *query = [PFUser query];
        [query whereKey:@"username" equalTo:_textFieldUserName.text];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
         {
             if (!error)
             {
                 NSLog(@"downloaded %lu objects", (unsigned long)[objects count]);
                 if ([objects count])
                 {
                     NSLog(@"Someone already has that user name!!");
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Login Error" message: @"Username already taken! Please try again with a different Username!" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alert show];
                 }
                 else
                 {
                     PFUser *newUser = [PFUser user];
                     newUser.username = _textFieldUserName.text;
                     newUser.password = _textFieldFirstPassword.text;
                     newUser[@"name"] = _textFieldHeroName.text;
                     newUser[alignment] = [NSNumber numberWithInteger:_segmentedHeroAlignment.selectedSegmentIndex];
                     
                     if (_buttonUserImageHere.tag == 2)
                     {
                         NSData* data = UIImageJPEGRepresentation(_buttonUserImageHere.imageView.image, 0.5f);
                         PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:data];
                         newUser[@"customUserImage"] = imageFile;
                     }
                     
                     [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                         if (!error)
                         {
                             [self dismissViewControllerAnimated:YES completion:nil];
                             NSLog(@"New user successfully created");
                         }
                         else
                         {
                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Login Error" message:[NSString stringWithFormat:@"%@", [error userInfo][@"error"]] delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                             [alert show];
                         }
                     }];
                 }
             }
             else
             {
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Login Error" message:[NSString stringWithFormat:@"%@", [error userInfo][@"error"]] delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 [alert show];
                 
                 NSLog(@"An error occurred! %@", error);
             }
         }];
    }
}

- (IBAction)pressedRegisterWithFacebook:(id)sender
{
    if ([_textFieldHeroName.text isEqualToString:@""])
    {
        _labelMustEnterHeroName.hidden = NO;
    }
    else
    {
        [PFFacebookUtils logInWithPermissions:nil block:^(PFUser *user, NSError *error)
         {
             if (!user)
             {
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Registration Error" message: @"Process was cancelled or error occurred registering, please try again or register manually!" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 [alert show];
             }
             else if (user.isNew)
             {
                 user[@"name"] = _textFieldHeroName.text;
                 user[alignment] = [NSNumber numberWithInteger:_segmentedHeroAlignment.selectedSegmentIndex];
                 if (_buttonUserImageHere.tag == 2)
                 {
                     NSData* data = UIImageJPEGRepresentation(_buttonUserImageHere.imageView.image, 0.5f);
                     PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:data];
                     user[@"customUserImage"] = imageFile;
                     //currentSettings.heroImage = _buttonUserImageHere.imageView.image;
                 }
                 [user saveInBackground];
                 NSLog(@"User signed up and logged in through Facebook!");
                 [self dismissViewControllerAnimated:YES completion:nil];
             }
             else
             {
                 user[@"name"] = _textFieldHeroName.text;
                 user[alignment] = [NSNumber numberWithInteger:_segmentedHeroAlignment.selectedSegmentIndex];
                 if (_buttonUserImageHere.tag == 2)
                 {
                     NSData* data = UIImageJPEGRepresentation(_buttonUserImageHere.imageView.image, 0.5f);
                     PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:data];
                     user[@"customUserImage"] = imageFile;
                 }
                 [user saveInBackground];
                 NSLog(@"User logged in through Facebook!");
                 [self dismissViewControllerAnimated:YES completion:nil];
             }
         }];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTextFieldHoldingKeyboard = textField;
}

- (IBAction)pressedInvsKeyboardDismiss:(id)sender
{
    [currentTextFieldHoldingKeyboard resignFirstResponder];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //sending back to login screen first because when logging out I dont want the registration screen to pop up, wanting the login screen to come up after dismissing the tableview controller, also to control flow of information for easier debugging
    if ([segue.identifier isEqualToString:@"imageSelection"])
    {
        CameraVC *cvc = [segue destinationViewController];
        cvc.delegate = self;
    }
}

@end
