//
//  QuestGiverAnnoView.m
//  Quest
//
//  Created by Greg Tropino on 5/9/14.
//
//

#import "QuestGiverAnnoView.h"

@implementation QuestGiverAnnoView


-(id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier giverOrLocation:(NSString*)questIsGiverOrLocation withQuest:(Quests *)passedQuest
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self)
    {
        if ([questIsGiverOrLocation isEqualToString:@"GiverLoc"])
        {
            if (passedQuest.questGiverImage)
            {
                UIGraphicsBeginImageContextWithOptions(CGSizeMake(20, 20), NO, 0.0);
                [passedQuest.questGiverImage drawInRect:CGRectMake(0, 0, 20, 20)];
                UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                self.image = newImage;
            }
            else
            {
                UIGraphicsBeginImageContextWithOptions(CGSizeMake(20, 20), NO, 0.0);
                [[UIImage imageNamed:@"QuestGiverImage.png"] drawInRect:CGRectMake(0, 0, 20, 20)];
                UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                self.image = newImage;
            }
        }
        else if ([questIsGiverOrLocation isEqualToString:@"QuestLoc"])
        {
            if (passedQuest.questImage)
            {
                UIGraphicsBeginImageContextWithOptions(CGSizeMake(20, 20), NO, 0.0);
                [passedQuest.questImage drawInRect:CGRectMake(0, 0, 20, 20)];
                UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                self.image = newImage;
            }
            else
            {
                UIGraphicsBeginImageContextWithOptions(CGSizeMake(20, 20), NO, 0.0);
                [[UIImage imageNamed:@"XMarkDestination.jpg"] drawInRect:CGRectMake(0, 0, 20, 20)];
                UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                self.image = newImage;
            }
        }
        else
        {
            NSLog(@"Something went wrong in the GiverAnnoView!!!!");
        }
    }
    return self;
}

@end
