//
//  Constants.h
//  Quest
//
//  Created by Greg Tropino on 5/20/14.
//
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

extern NSString *good;
extern NSString *neutral;
extern NSString *evil;
extern NSString *alignment;

@end
