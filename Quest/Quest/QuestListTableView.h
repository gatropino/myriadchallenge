//
//  QuestListTableView.h
//  Quest
//
//  Created by Greg Tropino on 5/6/14.
//
//

#import <UIKit/UIKit.h>
#import "SettingsVC.h"
#import "PlayerSettings.h"
#import "DetailQuestVC.h"

@interface QuestListTableView : UITableViewController <settingsDelegate, updateQuestStatusDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedQuestControl;
@property (strong, nonatomic) PlayerSettings *currentSettings;

- (IBAction)pressedSettings:(id)sender;
- (IBAction)segmentedQuestAvailabilityPressed:(id)sender;
- (IBAction)pressedLogOut:(id)sender;

@end
