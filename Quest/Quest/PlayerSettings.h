//
//  PlayerSettings.h
//  Quest
//
//  Created by Greg Tropino on 5/7/14.
//
//

#import <Foundation/Foundation.h>

@interface PlayerSettings : NSObject

@property (strong, nonatomic) NSString *loginName;
@property (strong, nonatomic) NSString *heroName;
@property (strong, nonatomic) NSString *heroAlignment;
@property (strong, nonatomic) NSString *heroLocation;
@property (strong, nonatomic) UIImage *heroImage;
//give player ability to change backgrounds, or change backgrounds based on alignment??


@end
