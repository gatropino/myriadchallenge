//
//  Quests.h
//  Quest
//
//  Created by Greg Tropino on 5/7/14.
//
//

#import <Foundation/Foundation.h>

@interface Quests : NSObject

@property (strong, nonatomic) NSString *questTitle;
@property (strong, nonatomic) NSString *alignment;
@property (strong, nonatomic) NSString *description;
@property (nonatomic) float questLocationX;
@property (nonatomic) float questLocationY;
@property (strong, nonatomic) NSString *questGiver;
@property (strong, nonatomic) NSString *questGiverImageLocation;
@property (strong, nonatomic) UIImage *questGiverImage;
@property (nonatomic) float questGiverLocationX;
@property (nonatomic) float questGiverLocationY;
@property (nonatomic) int questCurrentIndexOfArray;
@property (strong, nonatomic) NSString *questimageLocation;
@property (strong, nonatomic) UIImage *questImage;

//this value will be stored when downloaded quest from parse;
@property (strong, nonatomic) NSString *objectID;

//0 = Not Accepted, 1 = Accepted, 3 = Completed;
@property (nonatomic) int questAvailability;

+(NSMutableArray*)arrayWithPresetQuests;


@end
