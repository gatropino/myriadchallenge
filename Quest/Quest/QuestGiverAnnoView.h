//
//  QuestGiverAnnoView.h
//  Quest
//
//  Created by Greg Tropino on 5/9/14.
//
//

#import <MapKit/MapKit.h>
#import "Quests.h"

@interface QuestGiverAnnoView : MKAnnotationView

-(id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier giverOrLocation:(NSString*)questIsGiverOrLocation withQuest:(Quests*)passedQuest;

@end
