//
//  CustomTableViewCell.m
//  Quest
//
//  Created by Greg Tropino on 5/10/14.
//
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
/*
    //enable if you want to have table view cell to animate as well
    [CustomTableViewCell beginAnimations:nil context:NULL];
    [CustomTableViewCell setAnimationDuration:.25];
    [CustomTableViewCell setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self cache:YES];
    [CustomTableViewCell commitAnimations];
 */
}

@end
