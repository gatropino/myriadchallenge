//
//  Quests.m
//  Quest
//
//  Created by Greg Tropino on 5/7/14.
//
//

#import "Quests.h"
#import "Constants.h"

@implementation Quests

+(NSMutableArray*)arrayWithPresetQuests
{
    NSMutableArray *questArray = [[NSMutableArray alloc]init];
    
    Quests *tempQuest = [[Quests alloc]init];
    tempQuest.questTitle = @"Bandits in the Woods";
    tempQuest.alignment = good;
    tempQuest.description = @"The famed bounty hunter HotDog has requested the aid of a hero in ridding the woods of terrifying bandits who have thus far eluded his capture, as he is actually a dog, and cannot actually grab things more than 6 feet off the ground.";
    tempQuest.questLocationX = 46.908588;
    tempQuest.questLocationY = -96.808991;
    tempQuest.questGiver = @"HotDogg The Bounty Hunter";
    tempQuest.questGiverLocationX = 46.8541979;
    tempQuest.questGiverLocationY = -96.8285138;
    tempQuest.questAvailability = 0;
    tempQuest.objectID = @"1";
    [questArray addObject:tempQuest];
    
    tempQuest = [[Quests alloc]init];
    tempQuest.questTitle = @"Special Delivery";
    tempQuest.alignment = neutral;
    tempQuest.description = @"Sir Jimmy was once the fastest man in the kingdom, brave as any soldier and wise as a king. Unfortunately, age catches us all in the end, and he has requested that I, his personal scribe, find a hero to deliver a package of particular importance--and protect it with their life.";
    tempQuest.questLocationX = 46.8657639;
    tempQuest.questLocationY = -96.7363173;
    tempQuest.questGiver = @"Sir Jimmy The Swift";
    tempQuest.questGiverLocationX = 46.8739748;
    tempQuest.questGiverLocationY = -96.806112;
    tempQuest.questAvailability = 0;
    tempQuest.objectID = @"2";
    [questArray addObject:tempQuest];
    
    tempQuest = [[Quests alloc]init];
    tempQuest.questTitle = @"Filthy Mongrel";
    tempQuest.alignment = evil;
    tempQuest.description = @"That strange dog that everyone is treating like a bounty-hunter must go. By the order of Prince Jack, that smelly, disease ridden mongrel must be removed from our streets by any means necessary. He is disrupting the lives of ordinary citizens, and it's just really weird. Make it gone.";
    tempQuest.questLocationX = 46.892386;
    tempQuest.questLocationY = -96.799669;
    tempQuest.questGiver = @"Prince Jack, The Iron Horse";
    tempQuest.questGiverLocationX = 46.8739748;
    tempQuest.questGiverLocationY = -96.806112;
    tempQuest.questAvailability = 0;
    tempQuest.objectID = @"3";
    [questArray addObject:tempQuest];
    
    return questArray;
}

@end
