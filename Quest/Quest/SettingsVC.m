//
//  SettingsVC.m
//  Quest
//
//  Created by Greg Tropino on 5/7/14.
//
//

#import "SettingsVC.h"
#import <Parse/Parse.h>
#import "Constants.h"

@interface SettingsVC ()

@end

@implementation SettingsVC

- (void)viewDidLoad
{
    [super viewDidLoad];

    _textFieldName.text = _currentSettings.heroName;
    
    if (_currentSettings.heroImage)
    {
        _buttonYourImageHere.imageView.image = _currentSettings.heroImage;
    }
    
    if (_currentSettings.heroAlignment)
    {
        if ([_currentSettings.heroAlignment isEqualToString:good])
        {
            _segmentAlignmentBar.selectedSegmentIndex = 0;
        }
        else if ([_currentSettings.heroAlignment isEqualToString:neutral])
        {
            _segmentAlignmentBar.selectedSegmentIndex = 1;
        }
        else if ([_currentSettings.heroAlignment isEqualToString:evil])
        {
            _segmentAlignmentBar.selectedSegmentIndex = 2;
        }
    }
}


- (IBAction)pressedSave:(id)sender
{
    PFUser *currentUser = [PFUser currentUser];
    currentUser[@"name"] = _textFieldName.text;
    currentUser[alignment] = [NSNumber numberWithInteger:_segmentAlignmentBar.selectedSegmentIndex];
    if (_buttonYourImageHere.tag == 2)
    {
        NSData* data = UIImageJPEGRepresentation(_buttonYourImageHere.imageView.image, 0.5f);
        PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:data];
        currentUser[@"customUserImage"] = imageFile;
        _currentSettings.heroImage = _buttonYourImageHere.imageView.image;
        [_delegate passingNewHeroImage:_buttonYourImageHere.imageView.image];
    }
    [currentUser saveInBackground];
    [_delegate passingUserName:_textFieldName.text andAlignmentSelected:[_segmentAlignmentBar titleForSegmentAtIndex:_segmentAlignmentBar.selectedSegmentIndex]];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)pressedCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)pressedUpdateLocation:(id)sender
{
    //saw this button on wireframe, not sure what the purpose is, supposed to just display current location in a label next to it after pressed?
}

-(void)passedImageBack:(UIImage *)imagePassed
{
    _buttonYourImageHere.imageView.image = imagePassed;
    //assigning tag number to figure out if new image was inserted or not
    _buttonYourImageHere.tag = 2;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"cameraVC"])
    {
        CameraVC *cvc = [segue destinationViewController];
        cvc.delegate = self;
    }
}
@end
