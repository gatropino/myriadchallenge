//
//  CustomTableViewCell.h
//  Quest
//
//  Created by Greg Tropino on 5/10/14.
//
//
#import "CustomImageView.h"
#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *customTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *customDetailTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *customQuestXPText;
@property (weak, nonatomic) IBOutlet CustomImageView *customImage;

@end
