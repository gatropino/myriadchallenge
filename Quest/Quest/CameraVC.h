//
//  CameraVC.h
//  Quest
//
//  Created by Greg Tropino on 5/17/14.
//
//

#import <UIKit/UIKit.h>

@protocol passImageBackDelegate

-(void)passedImageBack:(UIImage*)imagePassed;

@end

@interface CameraVC : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) id <passImageBackDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *buttonUseThisPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)pressedTakePhoto:(id)sender;
- (IBAction)pressedSelectPhoto:(id)sender;
- (IBAction)pressedCancel:(id)sender;
- (IBAction)pressedUseThisPhoto:(id)sender;

@end
