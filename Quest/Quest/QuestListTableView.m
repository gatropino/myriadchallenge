//
//  QuestListTableView.m
//  Quest
//
//  Created by Greg Tropino on 5/6/14.
//
//

#import "QuestListTableView.h"
#import "Quests.h"
#import "CustomTableViewCell.h"
#import <Parse/Parse.h>
#import "Constants.h"

@interface QuestListTableView ()

@end

@implementation QuestListTableView
{
    NSMutableArray *standardQuests;
    NSMutableArray *filteredQuestArray;
    NSMutableArray *downloadedQuestArray;
    dispatch_queue_t imageQueue_;
}

- (void)viewDidLoad
{
    imageQueue_ = dispatch_queue_create("Image Queue", NULL);
    [super viewDidLoad];
    filteredQuestArray = [Quests arrayWithPresetQuests];
    standardQuests = [Quests arrayWithPresetQuests];
    [self loadParseQuests];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self questsToDisplayToTableView];
}

-(void)loadParseQuests
{
    downloadedQuestArray = [[NSMutableArray alloc]init];
    
    UIActivityIndicatorView *aiView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    aiView.hidesWhenStopped = YES;
    self.navigationItem.titleView = aiView;
    
    PFQuery *questQuery = [PFQuery queryWithClassName:@"Quests"];
    [questQuery includeKey:@"questGiver"];
    [questQuery includeKey:@"acceptedBy"];
    [questQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
     {
         for (PFObject *quest in objects)
         {
             Quests *downloadedQuest = [[Quests alloc]init];
             
             int numericalAlignmentValue = [quest[@"alignment"]intValue];
             if (numericalAlignmentValue == 0)
             {
                 downloadedQuest.alignment = good;
             }
             else if (numericalAlignmentValue == 1)
             {
                 downloadedQuest.alignment = neutral;
             }
             else if (numericalAlignmentValue == 2)
             {
                 downloadedQuest.alignment = evil;
             }
             
             downloadedQuest.questTitle = quest[@"name"];
             downloadedQuest.description = quest[@"description"];
             downloadedQuest.objectID = quest.objectId;
             
             //how to handle multiple users that have accepted the same quest???? Is it only one??? Am i overwriting this every time when saving status????
             PFUser *acceptedBy = quest[@"acceptedBy"];
             PFUser *currentUser = [PFUser currentUser];
             
             if ([acceptedBy.objectId isEqualToString:currentUser.objectId])
             {
                 downloadedQuest.questAvailability = 1;
             }
             else if ([quest[@"completed"]boolValue])
             {
                 downloadedQuest.questAvailability = 2;
             }
             else
             {
                 downloadedQuest.questAvailability = 0;
             }
             downloadedQuest.questimageLocation = quest[@"locationImageUrl"];
             
             PFGeoPoint *questLocation = quest[@"location"];
             downloadedQuest.questLocationX = questLocation.latitude;
             downloadedQuest.questLocationY = questLocation.longitude;
             
             PFUser *questGiver = quest[@"questGiver"];
             downloadedQuest.questGiver = questGiver[@"name"];
             downloadedQuest.questGiverImageLocation = questGiver[@"imageUrl"];
             
             PFGeoPoint *questGiverLocation = questGiver[@"location"];
             downloadedQuest.questGiverLocationX = questGiverLocation.latitude;
             downloadedQuest.questGiverLocationY = questGiverLocation.longitude;
             
             [downloadedQuestArray addObject:downloadedQuest];
         }
         [self questsToDisplayToTableView];
         
         //pulling all the images for the quest location and quest giver location seperately so that the text loads faster, not having to wait on the text AND images, is it worth potentially causing images not getting sent to detailQuestVC fast enough and not loading map pins??
         
         for (Quests *downloadedQuest in downloadedQuestArray)
         {
             dispatch_async(imageQueue_, ^{
                 NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:downloadedQuest.questimageLocation]];
                 dispatch_async(dispatch_get_main_queue(), ^
                                {
                                    downloadedQuest.questImage = [UIImage imageWithData:imageData];
                                    [self.tableView reloadData];
                                });
             });
         }
         
         for (Quests *downloadedQuest in downloadedQuestArray)
         {
             dispatch_async(imageQueue_, ^{
                 NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:downloadedQuest.questGiverImageLocation]];
                 dispatch_async(dispatch_get_main_queue(), ^
                                {
                                    downloadedQuest.questGiverImage = [UIImage imageWithData:imageData];
                                    [self.tableView reloadData];
                                });
             });
         }
         
         [self.tableView reloadData];
         /*
          for (Quests *downloadedQuest in downloadedQuestArray)
          {
          NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:downloadedQuest.questimageLocation] cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:10.0];
          [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
          {
          if (!error && data)
          {
          downloadedQuest.questImage = [UIImage imageWithData:data];
          [self.tableView reloadData];
          }
          }];
          }
          
          for (Quests *downloadedQuest in downloadedQuestArray)
          {
          NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:downloadedQuest.questGiverImageLocation] cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:10.0];
          [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
          {
          if (!error && data)
          {
          downloadedQuest.questGiverImage = [UIImage imageWithData:data];
          }
          }];
          }
          */
         self.navigationItem.titleView = nil;
     }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [filteredQuestArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"cell";
    
    CustomTableViewCell *customCell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (!customCell)
    {
        customCell = [[CustomTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
    }
    customCell.customTextLabel.text = ((Quests*)[filteredQuestArray objectAtIndex:indexPath.row]).questTitle;
    customCell.customDetailTextLabel.text = [NSString stringWithFormat:@"Posted By: %@", ((Quests*)[filteredQuestArray objectAtIndex:indexPath.row]).questGiver];
    customCell.customQuestXPText.text = @"Reward: 10,000 Gold 10,000 XP";
    customCell.customImage.image = ((Quests*)[filteredQuestArray objectAtIndex:indexPath.row]).questImage;
    
    /*
    if ([((Quests*)[filteredQuestArray objectAtIndex:indexPath.row]).questGiverImageLocation respondsToSelector:@selector(length)])
    {
        dispatch_async(imageQueue_, ^{
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:((Quests*)[filteredQuestArray objectAtIndex:indexPath.row]).questGiverImageLocation]];
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               customCell.customImage.image = [UIImage imageWithData:imageData];
                               [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                           });
        });
    }
     is this what they were looking for? Wouldn't it make too many data connections every time a cell got dequeued and repopulated???
    */
    
    return customCell;
}

#pragma filtering of arrays for whats displayed
-(void)questsToDisplayToTableView
{
    //visual changes according to alignment selected
    if ([_currentSettings.heroAlignment isEqualToString:good])
    {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"heaven.jpeg"] forBarMetrics:UIBarMetricsDefault];
    }
    else if ([_currentSettings.heroAlignment isEqualToString:evil])
    {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"hell.jpeg"] forBarMetrics:UIBarMetricsDefault];
    }
    else if ([_currentSettings.heroAlignment isEqualToString:neutral])
    {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nuetral.jpeg"] forBarMetrics:UIBarMetricsDefault];
    }
    
    if (_currentSettings.heroAlignment)
    {
        filteredQuestArray = [[NSMutableArray alloc] init];
        if ([_currentSettings.heroAlignment isEqualToString:neutral])
        {
            for (Quests *currentQuest in standardQuests)
            {
                if (currentQuest.questAvailability == _segmentedQuestControl.selectedSegmentIndex)
                {
                    [filteredQuestArray addObject:currentQuest];
                }
            }
            if (downloadedQuestArray)
            {
                for (Quests *quest in downloadedQuestArray)
                {
                    if (quest.questAvailability == _segmentedQuestControl.selectedSegmentIndex)
                    {
                        [filteredQuestArray addObject:quest];
                    }
                }
            }
        }
        else
        {
            for (Quests *currentQuest in standardQuests)
            {
                if ([currentQuest.alignment isEqualToString:_currentSettings.heroAlignment] && currentQuest.questAvailability == _segmentedQuestControl.selectedSegmentIndex)
                {
                    [filteredQuestArray addObject:currentQuest];
                }
            }
            if (downloadedQuestArray)
            {
                for (Quests *quest in downloadedQuestArray)
                {
                    if ([quest.alignment isEqualToString:_currentSettings.heroAlignment] && quest.questAvailability == _segmentedQuestControl.selectedSegmentIndex)
                    {
                        [filteredQuestArray addObject:quest];
                    }
                }
            }
        }
    }
    [self.tableView reloadData];
}

#pragma delegte methods
-(void)passingCurrentStateofQuest:(Quests *)questUpdated
{
    //((Quests*)[filteredQuestArray objectAtIndex:questUpdated.questCurrentIndexOfArray]).questAvailability = questUpdated.questAvailability;
    
    if (questUpdated.objectID.length > 2)
    {
        for (Quests *downloadedQuest in downloadedQuestArray)
        {
            if ([downloadedQuest.objectID isEqualToString:questUpdated.objectID])
            {
                downloadedQuest.questAvailability = questUpdated.questAvailability;
                break;
            }
        }
    }
    else
    {
        for (Quests *staticQuest in standardQuests)
        {
            if ([staticQuest.objectID isEqualToString:questUpdated.objectID])
            {
                staticQuest.questAvailability = questUpdated.questAvailability;
                break;
            }
        }
    }
    [self.tableView reloadData];
}

-(void)passingNewHeroImage:(UIImage *)heroImage
{
    _currentSettings.heroImage = heroImage;
}

-(void)passingUserName:(NSString *)userName andAlignmentSelected:(NSString *)currentAlignment
{
    _currentSettings.heroAlignment = currentAlignment;
    _currentSettings.heroName = userName;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"detailVC"])
    {
        DetailQuestVC *dqvc = [segue destinationViewController];
        dqvc.questReceived = [filteredQuestArray objectAtIndex:self.tableView.indexPathForSelectedRow.row];
        dqvc.questReceived.questCurrentIndexOfArray = (int)self.tableView.indexPathForSelectedRow.row;
        dqvc.currentSettings = _currentSettings;
        dqvc.delegate = self;
    }
    else if ([segue.identifier isEqualToString:@"settingsVC"])
    {
        SettingsVC *svc = [segue destinationViewController];
        svc.currentSettings = _currentSettings;
        svc.delegate = self;
    }
}

- (IBAction)pressedSettings:(id)sender
{
    [self performSegueWithIdentifier:@"settingsVC" sender:nil];
}

- (IBAction)segmentedQuestAvailabilityPressed:(id)sender
{
    [self questsToDisplayToTableView];
}

- (IBAction)pressedLogOut:(id)sender
{
    [PFUser logOut];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
