//
//  CustomImageView.m
//  Quest
//
//  Created by Greg Tropino on 5/18/14.
//
//

#import "CustomImageView.h"

@implementation CustomImageView

-(void)setImage:(UIImage *)image
{
    [super setImage:image];
    
    [CustomImageView beginAnimations:nil context:NULL];
    [CustomImageView setAnimationDuration:.75];
    [CustomImageView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self cache:YES];
    [CustomImageView commitAnimations];
}
@end
