//
//  DetailQuestVC.h
//  Quest
//
//  Created by Greg Tropino on 5/7/14.
//
//

#import <UIKit/UIKit.h>
#import "Quests.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PlayerSettings.h"

@protocol updateQuestStatusDelegate

-(void)passingCurrentStateofQuest:(Quests *)questUpdated;

@end

@interface DetailQuestVC : UIViewController <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelQuestTitle;
@property (weak, nonatomic) IBOutlet UITextView *textViewQuestGiver;
@property (weak, nonatomic) IBOutlet UITextView *textViewQuestDescription;

@property (strong, nonatomic) PlayerSettings *currentSettings;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) Quests *questReceived;
@property (weak, nonatomic) IBOutlet UIButton *buttonComplete;
@property (weak, nonatomic) IBOutlet UIButton *buttonAccept;
@property (strong, nonatomic) id <updateQuestStatusDelegate> delegate;

- (IBAction)pressedAcceptButton:(id)sender;
- (IBAction)pressedCompleteButton:(id)sender;

@end
