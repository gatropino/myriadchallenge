//
//  SettingsVC.h
//  Quest
//
//  Created by Greg Tropino on 5/7/14.
//
//

#import <UIKit/UIKit.h>
#import "PlayerSettings.h"
#import "CameraVC.h"

@protocol settingsDelegate

-(void)passingUserName:(NSString *)userName andAlignmentSelected:(NSString *)currentAlignment;
-(void)passingNewHeroImage:(UIImage*)heroImage;

@end

@interface SettingsVC : UIViewController <passImageBackDelegate>

@property (strong, nonatomic) PlayerSettings *currentSettings;
@property (weak, nonatomic) IBOutlet UITextField *textFieldName;
@property (weak, nonatomic) IBOutlet UILabel *labelCurrenLocation;
@property (strong, nonatomic) id <settingsDelegate> delegate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentAlignmentBar;
@property (weak, nonatomic) IBOutlet UIButton *buttonYourImageHere;

- (IBAction)pressedSave:(id)sender;
- (IBAction)pressedCancel:(id)sender;
- (IBAction)pressedUpdateLocation:(id)sender;

@end
