//
//  DetailQuestVC.m
//  Quest
//
//  Created by Greg Tropino on 5/7/14.
//
//

#import "DetailQuestVC.h"
#import "QuestAnnotationView.h"
#import "QuestGiverAnnoView.h"
#import <Parse/Parse.h>

@interface DetailQuestVC ()

@end

@implementation DetailQuestVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _labelQuestTitle.text = _questReceived.questTitle;
    _textViewQuestGiver.text = [NSString stringWithFormat:@"Quest Giver: %@", _questReceived.questGiver];
    _textViewQuestDescription.text = _questReceived.description;
    
    _textViewQuestGiver.font = [UIFont systemFontOfSize:18];
    _textViewQuestDescription.font = [UIFont systemFontOfSize:15];
    
    //rounding of image
    if (_currentSettings.heroImage)
    {
        _imageView.image = _currentSettings.heroImage;
        _imageView.layer.cornerRadius = _imageView.frame.size.height /2;
        _imageView.layer.masksToBounds = YES;
        _imageView.layer.borderWidth = 0;
    }
    //directions to individual points works on phone properly, but not simulator
    [self mapLoading];
}

-(void)viewDidAppear:(BOOL)animated
{
    if (_questReceived.questAvailability == 0)
    {
        _buttonAccept.hidden = NO;
    }
    else if (_questReceived.questAvailability == 1)
    {
        _buttonComplete.hidden = NO;
    }
    else
    {
        _buttonAccept.hidden = YES;
        _buttonComplete.hidden = YES;
        self.navigationController.navigationBar.topItem.title = @"Quest Completed!";
    }
}

-(void)mapLoading
{
    _mapView.mapType = 1;
    _mapView.zoomEnabled = YES;
    _mapView.scrollEnabled = YES;
    _mapView.showsUserLocation = NO;
    
    //initial coordinates and annotations getting setup
    CLLocationCoordinate2D questLocCoord = CLLocationCoordinate2DMake(_questReceived.questLocationX, _questReceived.questLocationY);
    CLLocationCoordinate2D questGivercoord = CLLocationCoordinate2DMake(_questReceived.questGiverLocationX, _questReceived.questGiverLocationY);
    QuestAnnotationView *questGiverView = [[QuestAnnotationView alloc]initWithTitle:_questReceived.questTitle subtitle:_questReceived.questGiver atCoodinates:questGivercoord giverOrLocation:@"GiverLoc"];
    QuestAnnotationView *questLocView = [[QuestAnnotationView alloc]initWithTitle:_questReceived.questTitle subtitle:_questReceived.questGiver atCoodinates:questLocCoord giverOrLocation:@"QuestLoc"];
    
    //coordinate inbetween the two locations to focus on
    float zoomInCoordX = (_questReceived.questLocationX + _questReceived.questGiverLocationX) / 2;
    float zoomInCoordY = (_questReceived.questLocationY + _questReceived.questGiverLocationY) / 2;
    CLLocationCoordinate2D zoomInCoord = CLLocationCoordinate2DMake(zoomInCoordX, zoomInCoordY);
    
    //getting distance between the two pin locations to then use in calculation for what zoom level should be
    CLLocation *questLocationCL = [[CLLocation alloc]initWithLatitude:_questReceived.questLocationX longitude:_questReceived.questLocationY];
    CLLocation *questGiverLocationCL = [[CLLocation alloc]initWithLatitude:_questReceived.questGiverLocationX longitude:_questReceived.questGiverLocationY];
    CLLocationDistance distance = [questLocationCL distanceFromLocation:questGiverLocationCL];
    double autoZoomCalculation = distance * .00001;
    MKCoordinateSpan span = MKCoordinateSpanMake(autoZoomCalculation, autoZoomCalculation);
    
    //actual modification/adding to the mapview
    [self.mapView addAnnotation:questGiverView];
    [self.mapView addAnnotation:questLocView];
    [self.mapView setRegion:MKCoordinateRegionMake(zoomInCoord, span)  animated:YES];
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(QuestAnnotationView*)annotation
{
    NSString *reuseID = @"abc";
    MKAnnotationView *view = [mapView dequeueReusableAnnotationViewWithIdentifier:reuseID];
    if (!view)
    {
        if ([annotation.questGiverOrLocation isEqualToString:@"GiverLoc"])
        {
            view = [[QuestGiverAnnoView alloc] initWithAnnotation:annotation reuseIdentifier:reuseID giverOrLocation:annotation.questGiverOrLocation withQuest:_questReceived];
            view.canShowCallout = YES;
            view.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            mapView.tag = 1;
            return view;
        }
        if ([annotation.questGiverOrLocation isEqualToString:@"QuestLoc"])
        {
            view = [[QuestGiverAnnoView alloc] initWithAnnotation:annotation reuseIdentifier:reuseID giverOrLocation:annotation.questGiverOrLocation withQuest:_questReceived];
            view.canShowCallout = YES;
            view.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            mapView.tag = 2;
            return view;
        }
    }
    else
    {
        view.annotation = annotation;
    }
    
    return view;
    
}
-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    if (mapView.tag == 1)
    {
        CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake(_questReceived.questLocationX, _questReceived.questLocationY);
        MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
        MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
        
        NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
        [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
        
        [endingItem openInMapsWithLaunchOptions:launchOptions];
    }
    else if (mapView.tag == 2)
    {
        CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake(_questReceived.questGiverLocationX, _questReceived.questGiverLocationY);
        MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
        MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
        
        NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
        [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
        
        [endingItem openInMapsWithLaunchOptions:launchOptions];
    }
    else
    {
        NSLog(@"Something went wrong with finding tag number and loading map app properly, tag it found was %ld", (long)mapView.tag);
    }
}


- (IBAction)pressedAcceptButton:(id)sender
{
    _questReceived.questAvailability = 1;
    _buttonAccept.hidden = YES;
    _buttonComplete.hidden = NO;
    
    [_delegate passingCurrentStateofQuest:_questReceived];
    
    if (_questReceived.objectID.length > 2)
    {
        //do you really just overwrite the last user to accept the quest???
        PFQuery *query = [PFQuery queryWithClassName:@"Quests"];
        [query getObjectInBackgroundWithId:_questReceived.objectID block:^(PFObject *quest, NSError *error)
         {
             quest[@"acceptedBy"] = [PFUser currentUser];
             [quest saveInBackground];
         }];
    }
}

- (IBAction)pressedCompleteButton:(id)sender
{
    //Do I do anything to parse when completing? Maybe remove accepted status???
    _questReceived.questAvailability = 2;
    _buttonComplete.hidden = YES;
    self.navigationController.navigationBar.topItem.title = @"Quest Completed!";
    
    [_delegate passingCurrentStateofQuest:_questReceived];
    /*
    if (_questReceived.objectID.length > 2)
    {
        //do you really just overwrite the last user to complete the quest for everyone accessing, how does it become un-complete????
        PFQuery *query = [PFQuery queryWithClassName:@"Quests"];
        [query getObjectInBackgroundWithId:_questReceived.objectID block:^(PFObject *quest, NSError *error)
         {
             quest[@"completed"] = @YES;
             [quest saveInBackground];
         }];
    }
    */
    //have a third button to re-open quest to set quest back to un-completed, maybe set acceptedBy to null when quest is completed as well so it wont potentially get queried as both accepted and completed?
}
@end
