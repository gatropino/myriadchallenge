//
//  QuestAnnotationView.m
//  Quest
//
//  Created by Greg Tropino on 5/9/14.
//
//

#import "QuestAnnotationView.h"

@implementation QuestAnnotationView

- (id)initWithTitle:(NSString *)title subtitle:(NSString *)questDescription atCoodinates:(CLLocationCoordinate2D)coord giverOrLocation:(NSString*)questIsGiverOrLocation;
{
    self = [super init];
    if (self)
    {
        _title = title;
        _subtitle = questDescription;
        _coordinate = coord;
        _questGiverOrLocation = questIsGiverOrLocation;
    }
    return self;
}

@end
