//
//  WelcomeVC.h
//  Quest
//
//  Created by Greg Tropino on 5/6/14.
//
//

#import <UIKit/UIKit.h>
#import "PlayerSettings.h"

@interface WelcomeVC : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelInvalidUser;
@property (weak, nonatomic) IBOutlet UILabel *labelInvalidPassword;
@property (weak, nonatomic) IBOutlet UITextField *textFieldUsername;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (weak, nonatomic) IBOutlet UIButton *buttonInvsForKeyboardDismiss;
@property (weak, nonatomic) IBOutlet UISwitch *switchRememberUsername;
@property (weak, nonatomic) IBOutlet UILabel *labelOnOffSwitchDisplay;

@property (strong, nonatomic) PlayerSettings *currentSettings;

- (IBAction)pressedLogIn:(id)sender;
- (IBAction)pressedInvsDismissKeyboard:(id)sender;
- (IBAction)changedStateOfSwitch:(UISwitch*)sender;
- (IBAction)pressedLoginWithFB:(id)sender;

@end
